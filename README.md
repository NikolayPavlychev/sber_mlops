sber_mlops
==============================
created by: **Nikolay Pavlychev**

email: **nikolaypavlychev@ya.ru**

description: **MLOps learning project**

for reviewer: **Solving tasks**

* Task 1. Шаблонизация. Python пакеты и CLI. Snakemake

  Сделано:
  1. Создать структуру каталогов для своего DS проекта (можно опираться на шаблон Cookiecutter)
  2.Произвести рефакторинг своего кода, разбив его на отдельные логические .py модули
  3.Залить отдельным коммитом с соответствующим описанием в ваш репозиторий

  commit https://gitlab.com/NikolayPavlychev/sber_mlops/-/commit/12577a02811edf48c44cdf86e8f80f181651024e


* Task 2. Codestyle, инструменты форматирования, линтеры

   Сделано:
   1. Установить локально стилистический линтер (выбрать на свое усмотрение). 
   2. Интегрировать линтер в свою IDE (если поддерживается)
   3. Проверить корректность форматирования установленным линтером и внести неучтенные ранее стилистические ошибки
   4.Установить и настроить (так, чтобы не было конфликтов с линтером) авто-форматер (на своё усмотрение). Предпочтительно интегрировать с IDE.
   5.Пользуясь авто-форматером и руками провести форматирование всего проекта для его соответствия codestyle и хорошим практикам

* Task 3. Хранение и версионирование кода/Отработать github-flow

  Сделано:
  1. Создать локальный репозиторий со своим проектом
  2. Настроить gitignore в соответствии со средой и технологическим стеком проекта
  3. Создать удаленный репозиторий (remote) 
  4. Синхронизировать его с локальным через SSH
  5. Перенести в него свой проект
  6. Настроить репозиторий под github-flow:
  7. заблокировать main от прямых push
  8. запретить merge без разрешения всех дискуссий
  9. Отработать github-flow
  10. Создать новую feature ветку и выполнить в нее коммит изменений
  11. Сделать Pull новой ветки в remote
  12. Инициировать merge/pull request
  13. Эмитировать code-review добавив комментарии
  14. Разрешить все комментарии
  15. Сделать approve merge request
  16. Удалить feature ветку

    commit https://gitlab.com/NikolayPavlychev/sber_mlops/-/merge_requests/2

    commit https://gitlab.com/NikolayPavlychev/sber_mlops/-/merge_requests/6

* Task 4. Gitlab CI

  Cделано:
  1. Настроить и подключить Runner

* Task 5. Python пакеты и CLI. Управление зависимостями. DVC

  Сделано:
  1. Реализовать CLI для основных модулей пайплайна
  2. Настроить DVC для хранения данных в S3 и для запуска пайплайна
  3. Сохранить зависимости проекта в соответствующем вашему менеджеру зависимостей формате

    commit https://gitlab.com/NikolayPavlychev/sber_mlops/-/merge_requests/8

* Task 6. Инструменты автоматизации ML исследований, DVC + MLFlow

  Сделано:
  1. Установить MLFlow в зависимости своего проекта
  2. Настроить логирование: параметров, метрик, моделей

    commit https://gitlab.com/NikolayPavlychev/sber_mlops/-/merge_requests/9


Project Organization
------------

    ├── LICENSE
    ├── Makefile           <- Makefile with commands like `make data` or `make train`
    ├── README.md          <- The top-level README for developers using this project.
    │
    ├── docs               <- A default Sphinx project; see sphinx-doc.org for details
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    ├── setup.py           <- makes project pip installable (pip install -e .) so src can be imported
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │   └── make_dataset.py
    │   │
    │   ├── features       <- Scripts to turn raw data into features for modeling
    │   │   └── build_features.py
    │   │
    │   ├── models         <- Scripts to train models and then use trained models to make
    │   │   │                 predictions
    │   │   ├── predict_model.py
    │   │   └── train_model.py
    │   │
    │   └── visualization  <- Scripts to create exploratory and results oriented visualizations
    │       └── visualize.py
    │
    └── tox.ini            <- tox file with settings for running tox; see tox.readthedocs.io


--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>
