import argparse
import pandas as pd
from transformers import AutoModelForCausalLM, AutoTokenizer, pipeline

parser = argparse.ArgumentParser(description='inference LLM on input prompts')
parser.add_argument('--model', type=str, default="./models/saiga_mistral_7b-GPTQ", help='model path')
parser.add_argument('--input_data_path', type=str, default="./src/data/prompts.txt", help='input data path')
args = parser.parse_args()

model_name_or_path = args.model
with open(args.input_data_path, 'r') as f:
    data = f.readlines()

input_data = pd.DataFrame(data, columns=['input_prompt'])

model = AutoModelForCausalLM.from_pretrained(model_name_or_path,
                                             device_map="auto",
                                             trust_remote_code=False,
                                             revision="main")

tokenizer = AutoTokenizer.from_pretrained(model_name_or_path, use_fast=True)

system_message = "Ты — Сайга, русскоязычный автоматический ассистент. Ты разговариваешь с людьми и помогаешь им. Отвечай только на поставленный вопрос"


def prompt_template(prompt):
    prompt_template=f'''<|im_start|>system{system_message}<|im_end|><|im_start|>user
                        {prompt}<|im_end|><|im_start|>assistant'''
    return prompt_template
   

print("\n\n*** Generate:")

# Inference can also be done using transformers' pipeline
def inference(prompt_template, text):
    prompt_template = prompt_template(text)
    pipe = pipeline(
    "text-generation",
    model=model,
    tokenizer=tokenizer,
    max_new_tokens=512,
    do_sample=True,
    temperature=0.7,
    top_p=0.95,
    top_k=40,
    repetition_penalty=1.1)
    
    answer = pipe(prompt_template)[0]['generated_text']
    if "bot" in answer:
        answer = answer.split("bot")[0].strip()
    return answer

input_data['response'] = input_data['input_prompt'].apply(lambda x: inference(prompt_template, x))
input_data.to_csv("./src/data/responces.csv", sep='|', index=False)
print("Generation responces completed and saved in ./src/data/responces.csv")
